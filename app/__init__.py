from flask import Flask, render_template, flash, request, url_for, redirect
from flask_bootstrap import Bootstrap
from flask_appconfig import AppConfig
from flask import redirect, url_for
from app.congress_stats import CongressStats #can be removed once sourcing from database
from app.gitlab_stats import GitlabStats
from app.senator_stats import SenatorStats #can be removed once sourcing from database
from app.create_db import app, db, Senator, create_sens, State, create_states, Party, create_party_stats
from app.search import SearchForm, render_search_string
from sqlalchemy import or_
import pandas
import subprocess
import os

def party_format_helper(sen, house):
    print('here')
    if sen and house:
        return 'Senate, House'
    if sen:
        return 'Senate'
    if house:
        return 'House'
    return 'None'

def create_app(configfile=None):
    #app = Flask(__name__)
    AppConfig(app, configfile)
    Bootstrap(app)
    app.secret_key = os.urandom(24)

    @app.route('/', methods=['GET', 'POST'])
    def index():
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)

        return render_template('index.html', form=search)

    @app.route('/results', methods=['GET', 'POST'])
    def search_results(search):
        results = []
        search_string = search.data['search']
        senDict = render_search_string(search_string)

        return render_template('results.html', senDict = senDict, form=search)


    @app.route('/map', methods=['GET', 'POST'])
    def map():
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)

        return render_template('map.html', form=search)

    @app.route('/about', methods=['GET', 'POST'])
    def about():
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)

        stats = GitlabStats()
        project_stats = stats.get_project_stats()
        member_stats = stats.get_member_stats()
        return render_template('about.html', project_stats=project_stats, member_stats=member_stats, form=search)

    @app.route('/run_tests', methods=['GET', 'POST'])
    def test():
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)

        p = subprocess.Popen(["coverage", "run", "--branch", "app/tests.py"],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                stdin=subprocess.PIPE)
        out, err = p.communicate()
        output=err+out
        output = output.decode("utf-8") #convert from byte type to string type
        # Repopulate the databases
        create_sens()
        create_states()
        create_party_stats()
        return render_template('run_tests.html', output = output.split("\n"), form=search)

    @app.route('/party', methods=['GET', 'POST'])
    def party():
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)

        return render_template('party_main.html', form=search)

    @app.route('/<partyname>', methods=['GET', 'POST'])
    def indiv_party(partyname):
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)

        q = db.session.query(Party.Name,
                                 Party.NumSenators,
                                 Party.NumReps,
                                 Party.NumFemale,
                                 Party.NumMale,
                                 Party.ControlSenate,
                                 Party.ControlHouse
                                )

        if partyname == 'Rep':
            stats = {
                'num_senators': q[0][1],
                'num_reps': q[0][2],
                'num_female': q[0][3],
                'num_male': q[0][4],
                'chambers_controlled': party_format_helper(q[0][5], q[0][6]),
            }
            name = 'Republican Party'
            senDict = render_search_string('republican')
        elif partyname == 'Dem':
            stats = {
                'num_senators': q[1][1],
                'num_reps': q[1][2],
                'num_female': q[1][3],
                'num_male': q[1][4],
                'chambers_controlled': party_format_helper(q[1][5], q[1][6]),
            }
            name = 'Democratic Party'
            senDict = render_search_string('democratic')
        elif partyname == 'ID':
            stats = {
                'num_senators': q[2][1],
                'num_reps': q[2][2],
                'num_female': q[2][3],
                'num_male': q[2][4],
                'chambers_controlled': party_format_helper(q[2][5], q[2][6]),
            }
            name = 'Third Parties'
            senDict = render_search_string('third party')
        else:
            return redirect(url_for('index'))
        return render_template('party.html', stats=stats, name=name, senDict=senDict, form=search)

    @app.route('/senators', methods=['GET', 'POST'])
    def senators():
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)

        query = db.session.query(Senator.Name, Senator.State, Senator.Party, Senator.Years, Senator.Reelection, Senator.Website)
        senDict = {}
        for i in range(19):
            info = []
            for j in range(1, 6):
                info.append(query[i][j])
            senDict[query[i][0]] = info

        return render_template('senators.html', senDict = senDict, form=search)


    @app.route('/senatorsDem', methods=['GET', 'POST'])
    def senators_dem():
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)
        query = db.session.query(Senator.Name, Senator.State, Senator.Party, Senator.Years, Senator.Reelection, Senator.Website)
        senDict = {}
        count = 0
        for i in range(100):
            if(query[i][2] == 'D'):
                info = []
                count += 1
                for j in range(1, 6):
                    info.append(query[i][j])
                    senDict[query[i][0]] = info
                if(count >= 9):
                    break
        
        return render_template('senatorsDem.html', senDict = senDict, form=search)


    @app.route('/senatorsDem2', methods=['GET', 'POST'])
    def senators_dem2():
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)
        query = db.session.query(Senator.Name, Senator.State, Senator.Party, Senator.Years, Senator.Reelection, Senator.Website)
        senDict = {}
        count = 0
        for i in range(100):
            if(query[i][2] == 'D'):
                count += 1
                if(count <= 9):
                    continue
                info = []
                for j in range(1, 6):
                    info.append(query[i][j])
                    senDict[query[i][0]] = info
                if(count >= 18):
                    break
        
        return render_template('senatorsDem2.html', senDict = senDict, form=search)


    @app.route('/senatorsDem3', methods=['GET', 'POST'])
    def senators_dem3():
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)
        query = db.session.query(Senator.Name, Senator.State, Senator.Party, Senator.Years, Senator.Reelection, Senator.Website)
        senDict = {}
        count = 0
        for i in range(100):
            if(query[i][2] == 'D'):
                count += 1
                if(count <= 18):
                    continue
                info = []
                for j in range(1, 6):
                    info.append(query[i][j])
                    senDict[query[i][0]] = info
                if(count >= 27):
                    break
        
        return render_template('senatorsDem3.html', senDict = senDict, form=search)


    @app.route('/senatorsDem4', methods=['GET', 'POST'])
    def senators_dem4():
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)
        query = db.session.query(Senator.Name, Senator.State, Senator.Party, Senator.Years, Senator.Reelection, Senator.Website)
        senDict = {}
        count = 0
        for i in range(100):
            if(query[i][2] == 'D'):
                count += 1
                if(count <= 27):
                    continue
                info = []
                for j in range(1, 6):
                    info.append(query[i][j])
                    senDict[query[i][0]] = info
                if(count >= 36):
                    break
        
        return render_template('senatorsDem4.html', senDict = senDict, form=search)


    @app.route('/senatorsDem5', methods=['GET', 'POST'])
    def senators_dem5():
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)
        query = db.session.query(Senator.Name, Senator.State, Senator.Party, Senator.Years, Senator.Reelection, Senator.Website)
        senDict = {}
        count = 0
        for i in range(100):
            if(query[i][2] == 'D'):
                count += 1
                if(count <= 36):
                    continue
                info = []
                for j in range(1, 6):
                    info.append(query[i][j])
                    senDict[query[i][0]] = info
                if(count >= 45):
                    break
        
        return render_template('senatorsDem5.html', senDict = senDict, form=search)


    @app.route('/senatorsRep', methods=['GET', 'POST'])
    def senators_rep():
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)

        query = db.session.query(Senator.Name, Senator.State, Senator.Party, Senator.Years, Senator.Reelection, Senator.Website)
        senDict = {}
        count = 0
        for i in range(100):
            if(query[i][2] == 'R'):
                info = []
                count += 1
                for j in range(1, 6):
                    info.append(query[i][j])
                    senDict[query[i][0]] = info
                if(count >= 11):
                    break

        return render_template('senatorsRep.html', senDict = senDict, form=search)


    @app.route('/senatorsRep2', methods=['GET', 'POST'])
    def senators_rep2():
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)

        query = db.session.query(Senator.Name, Senator.State, Senator.Party, Senator.Years, Senator.Reelection, Senator.Website)
        senDict = {}
        count = 0
        for i in range(100):
            if(query[i][2] == 'R'):
                count += 1
                if(count <= 11):
                    continue
                info = []
                for j in range(1, 6):
                    info.append(query[i][j])
                    senDict[query[i][0]] = info
                if(count >= 22):
                    break

        return render_template('senatorsRep2.html', senDict = senDict, form=search)


    @app.route('/senatorsRep3', methods=['GET', 'POST'])
    def senators_rep3():
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)

        query = db.session.query(Senator.Name, Senator.State, Senator.Party, Senator.Years, Senator.Reelection, Senator.Website)
        senDict = {}
        count = 0
        for i in range(100):
            if(query[i][2] == 'R'):
                count += 1
                if(count <= 22):
                    continue
                info = []
                for j in range(1, 6):
                    info.append(query[i][j])
                    senDict[query[i][0]] = info
                if(count >= 33):
                    break

        return render_template('senatorsRep3.html', senDict = senDict, form=search)


    @app.route('/senatorsRep4', methods=['GET', 'POST'])
    def senators_rep4():
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)

        query = db.session.query(Senator.Name, Senator.State, Senator.Party, Senator.Years, Senator.Reelection, Senator.Website)
        senDict = {}
        count = 0
        for i in range(100):
            if(query[i][2] == 'R'):
                count += 1
                if(count <= 33):
                    continue
                info = []
                for j in range(1, 6):
                    info.append(query[i][j])
                    senDict[query[i][0]] = info
                if(count >= 44):
                    break

        return render_template('senatorsRep4.html', senDict = senDict, form=search)


    @app.route('/senatorsRep5', methods=['GET', 'POST'])
    def senators_rep5():
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)

        query = db.session.query(Senator.Name, Senator.State, Senator.Party, Senator.Years, Senator.Reelection, Senator.Website)
        senDict = {}
        count = 0
        for i in range(100):
            if(query[i][2] == 'R'):
                count += 1
                if(count <= 44):
                    continue
                info = []
                for j in range(1, 6):
                    info.append(query[i][j])
                    senDict[query[i][0]] = info
                if(count >= 53):
                    break

        return render_template('senatorsRep5.html', senDict = senDict, form=search)
    

    @app.route('/senatorsID', methods=['GET', 'POST'])
    def senators_id():
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)

        query = db.session.query(Senator.Name, Senator.State, Senator.Party, Senator.Years, Senator.Reelection, Senator.Website)
        senDict = {}
        for i in range(100):
            info = []
            for j in range(1, 6):
                info.append(query[i][j])
            senDict[query[i][0]] = info

        return render_template('senatorsID.html', senDict = senDict, form=search)

    @app.route('/senators2', methods=['GET', 'POST'])
    def senators_table1():
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)

        query = db.session.query(Senator.Name, Senator.State, Senator.Party, Senator.Years, Senator.Reelection, Senator.Website)
        senDict = {}
        for i in range(19, 39):
            info = []
            for j in range(1, 6):
                info.append(query[i][j])
            senDict[query[i][0]] = info

        return render_template('senators2.html', senDict = senDict, form=search)

    @app.route('/senators3', methods=['GET', 'POST'])
    def senators_table2():
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)

        query = db.session.query(Senator.Name, Senator.State, Senator.Party, Senator.Years, Senator.Reelection, Senator.Website)
        senDict = {}
        for i in range(39, 59):
            info = []
            for j in range(1, 6):
                info.append(query[i][j])
            senDict[query[i][0]] = info

        return render_template('senators3.html', senDict = senDict, form=search)

    @app.route('/senators4', methods=['GET', 'POST'])
    def senators_table3():
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)

        query = db.session.query(Senator.Name, Senator.State, Senator.Party, Senator.Years, Senator.Reelection, Senator.Website)
        senDict = {}
        for i in range(59, 79):
            info = []
            for j in range(1, 6):
                info.append(query[i][j])
            senDict[query[i][0]] = info

        return render_template('senators4.html', senDict = senDict, form=search)

    @app.route('/senators5', methods=['GET', 'POST'])
    def senators_table4():
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)

        query = db.session.query(Senator.Name, Senator.State, Senator.Party, Senator.Years, Senator.Reelection, Senator.Website)
        senDict = {}
        for i in range(79, 99):
            info = []
            for j in range(1, 6):
                info.append(query[i][j])
            senDict[query[i][0]] = info

        return render_template('senators5.html', senDict = senDict, form=search)


    @app.route('/state', methods=['GET','POST'])
    # Example of how to feed in a parameter
    def state_eval():
        #search = SearchForm(request.form)
        #if request.method == 'POST':
        #     return search_results(search)

        statename = request.form.get('stateval')
        url = "/state/" + str(statename)
        return redirect(url)



    @app.route('/state/<statename>', methods=['GET','POST'])
    # Example of how to feed in a parameter
    def state(statename):
        search = SearchForm(request.form)
        if request.method == 'POST':
             return search_results(search)

        state_query = db.session.query(State.StateFull, State.StateAbbrev, State.StatePopulation, State.StateBird, State.StateFlower, State.StateParty, State.StateSen1, State.StateSen2).filter(State.StateAbbrev == statename).all()

        state_info = [s._asdict() for s in state_query]
        state_details = state_info[0]

        sfullname = state_details['StateFull']
        if len(sfullname.split()) == 2:
            sfull = sfullname.split()
            sfullname = '-'.join(sfull)
        flag_url = "https://www.states101.com/img/flags/svg/" + sfullname.lower() + ".svg"
        quarter_url = "https://www.states101.com/img/quarters/large/" + sfullname.lower() + ".png"

        sen1_query = db.session.query(Senator.Name, Senator.Rank, Senator.Party, Senator.Reelection, Senator.FB, Senator.Twitter, Senator.Website).filter(Senator.Name == state_details['StateSen1']).all()
        sen1_info = [s._asdict() for s in sen1_query]
        sen2_query = db.session.query(Senator.Name, Senator.Rank, Senator.Party, Senator.Reelection, Senator.FB, Senator.Twitter, Senator.Website).filter(Senator.Name == state_details['StateSen2']).all()
        sen2_info = [s._asdict() for s in sen2_query]
        sen_detail = {'Sen1': sen1_info[0], 'Sen2': sen2_info[0]}

        return render_template('state.html', state = statename, sdetail=state_details, detail=sen_detail, stateflag=flag_url, quarter=quarter_url, form=search)



    @app.route('/senator/<senname>', methods=['GET','POST'])
    # Example of how to feed in a parameter
    def sen_indiv(senname):
        search = SearchForm(request.form)
        if request.method == 'POST':
            return search_results(search)

        sen_query = db.session.query(Senator.Name, Senator.Rank, Senator.Party, Senator.Reelection, Senator.FB, Senator.Twitter, Senator.Website).filter(Senator.Name == senname).all()

        sen_info = [s._asdict() for s in sen_query]

        sen_detail = {'Sen': sen_info[0]}

        state_query = db.session.query(State.StateFull, State.StateAbbrev).filter(or_(State.StateSen1 == senname, State.StateSen2 == senname)).all()
        print(state_query)
        state_full = state_query[0][0]
        state_abbrev = state_query[0][1]

        return render_template('senator_indiv.html', state = state_full, state_abbrev = state_abbrev, detail=sen_detail, form=search)



    return app

if __name__ == '__main__':
    create_app().run(debug=True)
