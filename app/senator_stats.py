import requests
import yaml
import operator

class SenatorStats:
    def __init__(self):
        f = open('app/api_creds.yml', 'r')
        creds = yaml.safe_load(f)
        self.creds = creds['propublica']
        f.close()

        self.members = {
            'senate': self.get_members('senate')
        }

        self.senators = self.get_senator_info(self.members['senate'])
    
    def get_members(self, chamber):

        url = "https://api.propublica.org/congress/v1/116/{}/members.json?in_office=True".format(chamber)
        headers = {
                    'X-API-Key': self.creds['key'],
                    'content-type' : 'application/json'
                    }
        results = requests.get(url, headers=headers).json()['results']
        members = results[0]['members']

        return members

    def get_senator_info(self, members):
        senators = {}
        for m in members:
            name, state, politicalParty, years, reelection = m['first_name'] + ' ' + m['last_name'], m['state'], m['party'], m['seniority'], m['next_election']
            senatorInfo = []
            senatorInfo.append(state)
            senatorInfo.append(politicalParty)
            senatorInfo.append(years)
            senatorInfo.append(reelection)

            senators[name] = senatorInfo

        return senators

    def showInfo(self):
        return self.senators

if __name__=='__main__':
    main()


