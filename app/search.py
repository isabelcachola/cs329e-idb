from wtforms import Form, StringField, SelectField
from app.create_db import app, db, Senator, create_sens, State, create_states, Party, create_party_stats
from flask_table import Table, Col

class SearchForm(Form):
    search = StringField('search bar',
        render_kw={
            "placeholder": "Search...",
            "class":"form-control"}
            )

def render_search_string(search_string):
    # Query databases
    search_string = search_string.strip()
    # Search for exact match to abbreviation
    qry = db.session.query(Senator).filter(
        Senator.State.__eq__(search_string.upper())
    )
    results = qry.count()

    # Check if inputed full name of state
    if not results:
        state_qry = db.session.query(State).filter(
            State.StateFull.__eq__(search_string.lower().title()))
        state_count = state_qry.count()

        if state_count > 0:
            # state_qry = state_qry.one()
            state = state_qry.one().StateAbbrev
            qry = db.session.query(Senator).filter(
                Senator.State.__eq__(state)
            )
            results = qry.count()
    # Check if input is a party name
    if not results:
        s = search_string.lower()
        if s == "republican":
            qry = db.session.query(Senator).filter(
                Senator.Party.__eq__('R')
            )
            results = qry.count()
        elif s == 'democratic' or s == 'democrat':
            qry = db.session.query(Senator).filter(
                Senator.Party.__eq__('D')
            )
            results = qry.count()
        elif s == 'independent' or s == 'third party':
            qry = db.session.query(Senator).filter(
                Senator.Party.__eq__('ID')
            )
            results = qry.count()

    # Check if inputed senator Name
    if not results:
        s = search_string.title()
        qry = db.session.query(Senator).filter(
            Senator.Name.contains(s)
        )
        results = qry.count()
    # Display results
    if not results:
        return {}
    else:
        senDict = {}
        for q in qry:
            senDict[q.Name] = [
                q.State,
                q.Party,
                q.Rank,
                q.Years,
                q.Reelection
            ]
        return senDict
