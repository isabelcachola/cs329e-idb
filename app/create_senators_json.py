import json
import requests
import yaml
import operator

class SenatorStats:

    def __init__(self):
        f = open('app/api_creds.yml', 'r')
        creds = yaml.safe_load(f)
        self.creds = creds['propublica']
        f.close()
        self.members = {
            'senate': self.get_members('senate')
        }

        self.senators = self.get_senator_info(self.members['senate'])
        
        self.create_sen_json(self.senators) #creates senators.json file
        self.create_state_json() #creates states.json file
        
    
    def get_members(self, chamber):

        url = "https://api.propublica.org/congress/v1/116/{}/members.json?in_office=True".format(chamber)
        headers = {
                    'X-API-Key': self.creds['key'],
                    'content-type' : 'application/json'
                    }
        results = requests.get(url, headers=headers).json()['results']
        members = results[0]['members']

        return members
        

    def get_senator_info(self, members):
        senators = {}
        for m in members:
            name, state, party, rank, years, reelection, fb, twitter, website = m['first_name'] + ' ' + m['last_name'], m['state'], m['party'], m['state_rank'], m['seniority'], m['next_election'], m['facebook_account'], m['twitter_account'], m['url']
            
            if name == "Johnny Isakson":
                fb = "isakson"
            if name == "Rick Scott":
                fb = "scottforflorida"
            if name == "John Thune":
                fb = "johnthune"
            if name == "Cory Booker":
                fb = "corybooker"
            if name == "Mitt Romney":
                fb = "mittromney"    
            if name == "Martha McSally":
                fb = "SenMarthaMcSally"
                twitter = "senmcsallyaz"
            if name == "Kyrsten Sinema":
                fb = "KyrstenSinema" 
            if name == "Mike Braun":
                fb = "mikebraunforindiana"     
            if name == "Bill Cassidy":
                fb = "SenBillCassidy" 
                twitter = "SenBillCassidy"
            if name == "Amy Klobuchar":
                fb = "amyklobuchar"                 
            if name == "Cindy Hyde-Smith":
                fb = "CindyHydeSmith2018"                
            if name == "Roger Wicker":
                fb = "wicker"    
            if name == "Joshua Hawley":
                fb = "SenatorHawley"  
            if name == "Kevin Cramer":
                fb = "SenatorKevinCramer"  
            if name == "Ron Wyden":
                fb = "wyden" 
            if name == "Patty Murray":
                fb = "pattymurray" 
            if name == "Michael Bennet":
                twitter = "SenatorBennet" 
            if name == "Tim Kaine":
                twitter = "timkaine" 
                
            senatorInfo = []
            senatorInfo.append(state)
            senatorInfo.append(party)
            senatorInfo.append(rank)
            senatorInfo.append(years)
            senatorInfo.append(reelection)
            senatorInfo.append(fb)
            senatorInfo.append(twitter)
            senatorInfo.append(website)

            senators[name] = senatorInfo
        return senators
        
        
    def create_sen_json(self,senators):
    
        with open('senators.json', 'w') as fp:
            json.dump(senators, fp, indent=4)
    
    def create_state_json(self):
    
        state_dict = {"AL":"Alabama","AK":"Alaska","AZ":"Arizona","AR":"Arkansas","CA":"California","CO":"Colorado","CT":"Connecticut","DE":"Delaware","FL":"Florida","GA":"Georgia","HI":"Hawaii","ID":"Idaho","IL":"Illinois","IN":"Indiana","IA":"Iowa","KS":"Kansas","KY":"Kentucky","LA":"Louisiana","ME":"Maine","MD":"Maryland","MA":"Massachusetts","MI":"Michigan","MN":"Minnesota","MS":"Mississippi","MO":"Missouri","MT":"Montana","NE":"Nebraska","NV":"Nevada","NH":"New Hampshire","NJ":"New Jersey","NM":"New Mexico","NY":"New York","NC":"North Carolina","ND":"North Dakota","OH":"Ohio","OK":"Oklahoma","OR":"Oregon","PA":"Pennsylvania","RI":"Rhode Island","SC":"South Carolina","SD":"South Dakota","TN":"Tennessee","TX":"Texas","UT":"Utah","VT":"Vermont","VA":"Virginia","WA":"Washington","WV":"West Virginia","WI":"Wisconsin","WY":"Wyoming"}
        
        with open('states.json', 'w') as fp:
            json.dump(state_dict, fp, indent=4)
            
            
if __name__=='__main__':
    SenatorStats()

