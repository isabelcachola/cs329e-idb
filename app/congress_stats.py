import requests
import yaml
import operator
import json

class CongressStats:
    def __init__(self):
        f = open('app/api_creds.yml', 'r')
        creds = yaml.safe_load(f)
        self.creds = creds['propublica']
        f.close()

        self.members = {
            'house': self.get_members('house'),
            'senate': self.get_members('senate')
        }

        house_gender_stats, house_party_stats = self.get_stats(self.members['house'])
        senate_gender_stats, senate_party_stats = self.get_stats(self.members['senate'])

        self.gender_stats = {
            'house': house_gender_stats,
            'senate': senate_gender_stats
        }

        self.party_stats = {
            'house': house_party_stats,
            'senate': senate_party_stats
        }

    def get_members(self, chamber):
        url = "https://api.propublica.org/congress/v1/116/{}/members.json?in_office=True".format(chamber)
        headers = {
                    'X-API-Key': self.creds['key'],
                    'content-type' : 'application/json'
                    }
        results = requests.get(url, headers=headers).json()['results']
        members = results[0]['members']
        return members

    def get_stats(self, members):
        gender_count = {
            'R': {'M': 0, 'F':0},
            'D': {'M': 0, 'F':0},
            'ID': {'M': 0, 'F':0}
        }
        party_count = {
            'R': 0,
            'D': 0,
            'ID': 0
        }
        for m in members:
            gender, party = m['gender'], m['party']
            gender_count[party][gender] += 1
            party_count[party] += 1
        return gender_count, party_count

    def package_party_stats(self, p):
        packaged_party_stats = {
            'num_senators': self.party_stats['senate'][p],
            'num_reps': self.party_stats['house'][p],
            'num_female': 0,
            'num_male': 0,
            'control_house': 0,
            'control_senate': 0
        }
        for chamber in self.gender_stats:
            packaged_party_stats['num_male'] += self.gender_stats[chamber][p]['M']
            packaged_party_stats['num_female'] += self.gender_stats[chamber][p]['F']
            if p == max(self.party_stats[chamber].items(), key=operator.itemgetter(1))[0]:
                if chamber == 'house':
                    packaged_party_stats['control_house'] = 1
                elif chamber == 'senate':
                    packaged_party_stats['control_senate'] = 1

        return packaged_party_stats

    def create_party_json(self):
        party_dict = {
            'R': self.package_party_stats('R'),
            'D': self.package_party_stats('D'),
            'ID': self.package_party_stats('ID'),
        }
        with open('party.json', 'w') as fp:
            json.dump(party_dict, fp, indent=4)

if __name__=='__main__':
    cs = CongressStats()
    cs.create_party_json()
