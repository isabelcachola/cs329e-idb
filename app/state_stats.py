import requests
import yaml
import operator

class StateStats:
    def __init__(self):
        f = open('app/api_creds.yml', 'r')
        creds = yaml.safe_load(f)
        self.creds = creds['propublica']
        f.close()

        self.members = {
            'senate': self.get_members('senate')
        }
        
        self.sen_info, self.sen_bystate = self.sens_data(self.members['senate'])
        

    def get_members(self, chamber):
        url = "https://api.propublica.org/congress/v1/116/{}/members.json?in_office=True".format(chamber)
        headers = {
                    'X-API-Key': self.creds['key'],
                    'content-type' : 'application/json'
                  }
        results = requests.get(url, headers=headers).json()['results']
        members = results[0]['members']
        
        return members
        
    def sens_data(self, members):
        sens = []

        sen_info = {}
        sen_bystate = {}
        #Element 0 is state
        
        for x in members:
            fname = x['first_name']
            lname = x['last_name']
            name = fname + " " + lname
            state = x['state']
            party = x['party']
            rank = x['state_rank']
            next_elec = x['next_election']
            fb = x['facebook_account']
            twitter = x['twitter_account']
            
            if name == "Johnny Isakson":
                fb = "isakson"
            if name == "Rick Scott":
                fb = "scottforflorida"
            if name == "John Thune":
                fb = "johnthune"
            if name == "Cory Booker":
                fb = "corybooker"
            if name == "Mitt Romney":
                fb = "mittromney"    
            if name == "Martha McSally":
                fb = "SenMarthaMcSally"
                twitter = "senmcsallyaz"
            if name == "Kyrsten Sinema":
                fb = "KyrstenSinema" 
            if name == "Mike Braun":
                fb = "mikebraunforindiana"     
            if name == "Bill Cassidy":
                fb = "SenBillCassidy" 
                twitter = "SenBillCassidy"
            if name == "Amy Klobuchar":
                fb = "amyklobuchar"                 
            if name == "Cindy Hyde-Smith":
                fb = "CindyHydeSmith2018"                
            if name == "Roger Wicker":
                fb = "wicker"    
            if name == "Joshua Hawley":
                fb = "SenatorHawley"  
            if name == "Kevin Cramer":
                fb = "SenatorKevinCramer"  
            if name == "Ron Wyden":
                fb = "wyden" 
            if name == "Patty Murray":
                fb = "pattymurray" 
            if name == "Michael Bennet":
                twitter = "SenatorBennet" 
            if name == "Tim Kaine":
                twitter = "timkaine" 
 
                
            website = x['url']
            senator = []
            senator.append(state)
            senator.append(name)
            senator.append(party)
            senator.append(rank.capitalize())
            senator.append(next_elec)
            senator.append(fb)
            senator.append(website)
            senator.append(twitter)

            sen_info[name] = senator
            if state in sen_bystate:
                sen_bystate[state].append(name)
            else:
                sen_bystate[state] = [name]
                     
        
        return sen_info, sen_bystate
        
    def state_sen(self, state):
        
        print(self.sen_bystate)
        return self.sen_bystate[state]
        
    def get_sen_info(self, senator):
        
        info = {'Rank': self.sen_info[senator][3], 'Party': self.sen_info[senator][2], 'Year': self.sen_info[senator][4], 'FB': self.sen_info[senator][5], 'Website': self.sen_info[senator][6], 'Twitter': self.sen_info[senator][7]}
        return info
        
if __name__=='__main__':
    main()

