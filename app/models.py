from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:senator123@localhost:5432/senatordb')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)

class Senator(db.Model):

    __tablename__ = 'senators'
    Name = db.Column(db.String(120), primary_key = True)
    State = db.Column(db.String(80), nullable = False)
    Party = db.Column(db.String(80), nullable = False)
    Rank = db.Column(db.String(80), nullable = False)
    Years = db.Column(db.String(80), nullable = False)
    Reelection = db.Column(db.String(80), nullable = False)
    FB = db.Column(db.String(80), nullable = False)
    Twitter = db.Column(db.String(80), nullable = False)
    Website = db.Column(db.String(200), nullable = False)


class State(db.Model):
    __tablename__ = 'states'

    StateFull = db.Column(db.String(80), nullable = False)
    StateAbbrev = db.Column(db.String(2), primary_key = True)
    StatePopulation = db.Column(db.String(80), nullable = False)
    StateBird = db.Column(db.String(80), nullable = False)
    StateFlower = db.Column(db.String(80), nullable = False)
    StateParty = db.Column(db.String(80), nullable = False)
    StateSen1 = db.Column(db.String(120), nullable = False)
    StateSen2 = db.Column(db.String(120), nullable = False)


class Party(db.Model):
    __tablename__ = 'party'

    Name = db.Column(db.String(80), primary_key = True)
    NumSenators = db.Column(db.Integer, nullable = False)
    NumReps = db.Column(db.Integer, nullable = False)
    NumMale =  db.Column(db.Integer, nullable = False)
    NumFemale = db.Column(db.Integer, nullable = False)
    ControlHouse = db.Column(db.Boolean, nullable = False)
    ControlSenate = db.Column(db.Boolean, nullable = False)

db.drop_all()
db.create_all()
