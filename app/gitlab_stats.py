import requests
import yaml
import pandas as pd

class GitlabStats:
    def __init__(self):
        try:
            f = open('app/api_creds.yml', 'r')
        except FileNotFoundError:
            f = open('./api_creds.yml', 'r')
        creds = yaml.safe_load(f)
        self.creds = creds['gitlab']
        f.close()

        self.url = "https://gitlab.com/api/v4/projects/11285340/"
        self.params = {
            'private_token': self.creds['personal_token'],
            'statistics': 'TRUE',
            'per_page': 100
        }

        # Update as we write tests
        self.num_unit_tests = 9

    def get_project_stats(self):


        project = requests.get(self.url, params=self.params).json()
        issues = requests.get(self.url + 'issues/', params=self.params).json()

        project_stats = {
            'num_commits': project['statistics']['commit_count'],
            'repo_url': project['web_url'],
            'issue_url': project['_links']['issues'],
            'wiki_url': 'https://gitlab.com/isabelcachola/cs329e-idb/wikis/home',
            'num_issues': len(issues),
            'num_unit_tests': self.num_unit_tests
        }
        return project_stats

    def get_member_stats(self):
        try:
            df = pd.read_csv('app/static/member_data.tsv', sep='\t', header=0)
        except FileNotFoundError:
            df = pd.read_csv('./static/member_data.tsv', sep='\t', header=0)
        names = df['NAME']
        df['NUM_COMMITS'] = 0
        df['NUM_ISSUES'] = 0
        commits = requests.get(self.url + 'repository/commits/', params=self.params).json()
        issues = requests.get(self.url + 'issues/', params=self.params).json()

        # Iterate through commits and count for each member (using email as key)
        for c in commits:
            df.loc[df.EMAIL == c['author_email'], 'NUM_COMMITS'] += 1

        # Iterate through issues and count for each member
        for i in issues:
            try:
                df.loc[df.NAME == i['assignee']['name'], 'NUM_ISSUES'] += 1
            except:
                pass

        # Make the names the dataframe index
        df = df.set_index("NAME", drop = True)
        #df = df.to_json(orient='index')

        return df


if __name__=='__main__':
    stats = GitlabStats()
    print(stats.get_project_stats())
    print(stats.get_member_stats())
