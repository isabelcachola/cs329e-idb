import os
import sys
import unittest
from models import db, Senator, State, Party
#from app.create_db import db, Senator, State


class DBTestCases(unittest.TestCase):

    def test_senator_insert_1(self):
        s = Senator(Name='John Doe', State='TX', Party='R', Rank='junior', Years='3', Reelection='2020', FB='SenDoe', Twitter='SenDoe', Website='www.senjohndoe.com')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Senator).filter_by(Name = 'John Doe').one()
        self.assertEqual(str(r.Name), 'John Doe')

        db.session.query(Senator).filter_by(Name = 'John Doe').delete()
        db.session.commit()

    def test_senator_insert_2(self):
        s = Senator(Name='Daenerys Targaryen', State='CA', Party='D', Rank='junior', Years='3', Reelection='2020', FB='DaenerysTargaryen', Twitter='DaenerysTargaryen', Website='www.daenerystargaryen.com')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Senator).filter_by(Name = 'Daenerys Targaryen').one()
        self.assertEqual(str(r.Name), 'Daenerys Targaryen')

        db.session.query(Senator).filter_by(Name = 'Daenerys Targaryen').delete()
        db.session.commit()

    def test_senator_update_1(self):
        s = Senator(Name='John Doe', State='TX', Party='R', Rank='junior', Years='3', Reelection='2020', FB='SenDoe', Twitter='SenDoe', Website='www.senjohndoe.com')
        db.session.add(s)
        db.session.commit()

        db.session.query(Senator).filter_by(Name = 'John Doe').update({'Rank':'senior'})

        r = db.session.query(Senator).filter_by(Name = 'John Doe').one()
        self.assertEqual(str(r.Rank), 'senior')
        db.session.commit()

        db.session.query(Senator).filter_by(Name = 'John Doe').delete()
        db.session.commit()

    def test_senator_update_2(self):
        s = Senator(Name='John Doe', State='TX', Party='R', Rank='junior', Years='3', Reelection='2020', FB='SenDoe', Twitter='SenDoe', Website='www.senjohndoe.com')
        db.session.add(s)
        db.session.commit()

        db.session.query(Senator).filter_by(Name = 'John Doe').update({'Years':'13'})

        r = db.session.query(Senator).filter_by(Name = 'John Doe').one()
        self.assertEqual(str(r.Years), '13')
        db.session.commit()

        db.session.query(Senator).filter_by(Name = 'John Doe').delete()
        db.session.commit()

    def test_senator_update_3(self):
        s = Senator(Name='Daenerys Targaryen', State='CA', Party='D', Rank='junior', Years='3', Reelection='2020', FB='DaenerysTargaryen', Twitter='DaenerysTargaryen', Website='www.daenerystargaryen.com')
        db.session.add(s)
        db.session.commit()

        db.session.query(Senator).filter_by(Name = 'Daenerys Targaryen').update({'Rank':'senior'})

        r = db.session.query(Senator).filter_by(Name = 'Daenerys Targaryen').one()
        self.assertEqual(str(r.Rank), 'senior')
        db.session.commit()

        db.session.query(Senator).filter_by(Name = 'Daenerys Targaryen').delete()
        db.session.commit()

    def test_senator_update_5(self):
        s = Senator(Name='Daenerys Targaryen', State='CA', Party='D', Rank='junior', Years='3', Reelection='2020', FB='DaenerysTargaryen', Twitter='DaenerysTargaryen', Website='www.daenerystargaryen.com')
        db.session.add(s)
        db.session.commit()

        db.session.query(Senator).filter_by(Name = 'Daenerys Targaryen').update({'Years':'8'})

        r = db.session.query(Senator).filter_by(Name = 'Daenerys Targaryen').one()
        self.assertEqual(str(r.Years), '8')
        db.session.commit()

        db.session.query(Senator).filter_by(Name = 'Daenerys Targaryen').delete()
        db.session.commit()


    def test_party_insert_1(self):
        newParty = Party(
            Name = 'Green Party',
            NumSenators = 2,
            NumReps = 4,
            NumMale =  2,
            NumFemale = 6,
            ControlHouse = 0,
            ControlSenate = 0
        )
        db.session.add(newParty)
        db.session.commit()


        r = db.session.query(Party).filter_by(Name = 'Green Party').one()
        self.assertEqual(str(r.Name), 'Green Party')

        db.session.query(Party).filter_by(Name = 'Green Party').delete()
        db.session.commit()

    def test_party_insert_2(self):
        newParty = Party(
            Name = 'Communist Party',
            NumSenators = 4,
            NumReps = 20,
            NumMale = 12 ,
            NumFemale = 12,
            ControlHouse = 0,
            ControlSenate = 0
        )
        db.session.add(newParty)
        db.session.commit()


        r = db.session.query(Party).filter_by(Name = 'Communist Party').one()
        self.assertEqual(str(r.Name), 'Communist Party')

        db.session.query(Party).filter_by(Name = 'Communist Party').delete()
        db.session.commit()

    def test_party_update_1(self):
        newParty = Party(
            Name = 'Green Party',
            NumSenators = 2,
            NumReps = 4,
            NumMale =  2,
            NumFemale = 6,
            ControlHouse = 0,
            ControlSenate = 0
        )
        db.session.add(newParty)
        db.session.commit()

        db.session.query(Party).filter_by(Name = 'Green Party').update({'ControlHouse':1})

        r = db.session.query(Party).filter_by(Name = 'Green Party').one()
        self.assertTrue(bool(r.ControlHouse))
        db.session.commit()

        db.session.query(Party).filter_by(Name = 'Green Party').delete()
        db.session.commit()

    def test_party_update_2(self):
        newParty = Party(
            Name = 'Green Party',
            NumSenators = 2,
            NumReps = 4,
            NumMale =  2,
            NumFemale = 6,
            ControlHouse = 0,
            ControlSenate = 0
        )
        db.session.add(newParty)
        db.session.commit()

        db.session.query(Party).filter_by(Name = 'Green Party').update({'NumReps': 10})

        r = db.session.query(Party).filter_by(Name = 'Green Party').one()
        self.assertEqual(int(r.NumReps), 10)
        db.session.commit()

        db.session.query(Party).filter_by(Name = 'Green Party').delete()
        db.session.commit()

    def test_party_update_3(self):
        newParty = Party(
            Name = 'Communist Party',
            NumSenators = 4,
            NumReps = 20,
            NumMale = 12 ,
            NumFemale = 12,
            ControlHouse = 0,
            ControlSenate = 0
        )
        db.session.add(newParty)
        db.session.commit()

        db.session.query(Party).filter_by(Name = 'Communist Party').update({'ControlSenate':1})

        r = db.session.query(Party).filter_by(Name = 'Communist Party').one()
        self.assertTrue(bool(r.ControlSenate))
        db.session.commit()

        db.session.query(Party).filter_by(Name = 'Communist Party').delete()
        db.session.commit()

    def test_state_insert_1(self):
        s = State(StateFull='London', StateAbbrev='LO', StateBird='Sparrow', StateFlower='Rose', StatePopulation='1', StateParty='Republican', StateSen1='Saamiha', StateSen2='John Doe')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(State).filter_by(StateFull = 'London').one()
        self.assertEqual(str(r.StateFull), 'London')

        db.session.query(State).filter_by(StateFull = 'London').delete()
        db.session.commit()

    def test_state_update_1(self):
        s = State(StateFull='London', StateAbbrev='LD', StateBird='Sparrow', StateFlower='Rose', StatePopulation='1', StateParty='Republican', StateSen1='Saamiha', StateSen2='John Doe')
        db.session.add(s)
        db.session.commit()

        db.session.query(State).filter_by(StateAbbrev='LD').update({'StateFull':'Londontown'})

        r = db.session.query(State).filter_by(StateAbbrev='LD').one()
        self.assertEqual(str(r.StateFull), 'Londontown')
        db.session.commit()

        db.session.query(State).filter_by(StateAbbrev='LD').delete()
        db.session.commit()

    def test_state_update_2(self):
        s = State(StateFull='America', StateAbbrev='AM', StateBird='Sparrow', StateFlower='Rose', StatePopulation='1', StateParty='Republican', StateSen1='Saamiha', StateSen2='John Doe')
        db.session.add(s)
        db.session.commit()

        db.session.query(State).filter_by(StateAbbrev='AM').update({'StateFull':'Amsterdam'})

        r = db.session.query(State).filter_by(StateAbbrev='AM').one()
        self.assertEqual(str(r.StateFull), 'Amsterdam')
        db.session.commit()

        db.session.query(State).filter_by(StateAbbrev='AM').delete()
        db.session.commit()

    def test_state_update_3(self):
        s = State(StateFull='Wolfsburg', StateAbbrev='WF', StateBird='Raven', StateFlower='Lavander', StatePopulation='12345', StateParty='Republican', StateSen1='Khaleesi', StateSen2='Jon Snow')
        db.session.add(s)
        db.session.commit()

        db.session.query(State).filter_by(StateAbbrev='WF').update({'StateFull':'Winterfell'})

        r = db.session.query(State).filter_by(StateAbbrev='WF').one()
        self.assertEqual(str(r.StateFull), 'Winterfell')
        db.session.commit()

        db.session.query(State).filter_by(StateAbbrev='WF').delete()
        db.session.commit()

if __name__ == '__main__':
    unittest.main()
