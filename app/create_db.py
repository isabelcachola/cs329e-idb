import json
from app.models import app, db, Senator, State, Party
import os

def load_json(filename):
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

def create_sens():
    sen_info = load_json('app/senators.json')

    for senator in sen_info:
        name = senator
        state = sen_info[senator][0]
        party = sen_info[senator][1]
        rank = sen_info[senator][2].capitalize()
        years = sen_info[senator][3]
        reelection = sen_info[senator][4]
        fb = sen_info[senator][5]
        twitter = sen_info[senator][6]
        website = sen_info[senator][7]

        newSen = Senator(Name=name, State=state, Party=party, Rank=rank, Years=years, Reelection=reelection, FB=fb, Twitter=twitter, Website=website)

        db.session.add(newSen)
        # commit the session to my DB.
        db.session.commit()

def create_states():
    state_info = load_json('app/stateInfo.json')

    for state in state_info:
        abbrev = state
        statename = state_info[state][0]
        statepop = state_info[state][1]
        statebird = state_info[state][2]
        stateflower = state_info[state][3]
        stateparty = state_info[state][4]
        statesen1 = state_info[state][5]
        statesen2 = state_info[state][6]
        

        newState = State(StateAbbrev=abbrev, StateFull=statename, StatePopulation=statepop, StateBird=statebird, StateFlower=stateflower, StateParty=stateparty, StateSen1=statesen1, StateSen2=statesen2)

        db.session.add(newState)
        # commit the session to my DB.
        db.session.commit()

def create_party_stats():
    party_info = load_json('app/party.json')
    for party in party_info:
        name = party
        num_senators = party_info[party]['num_senators']
        num_reps = party_info[party]['num_reps']
        num_male = party_info[party]['num_male']
        num_female = party_info[party]['num_female']
        control_house = party_info[party]['control_house']
        control_senate = party_info[party]['control_senate']

        newParty = Party(
            Name = name,
        	NumSenators = num_senators,
            NumReps = num_reps,
            NumMale =  num_male,
            NumFemale = num_female,
            ControlHouse = control_house,
            ControlSenate = control_senate
        )

        db.session.add(newParty)
        db.session.commit()


create_sens()
create_states()
create_party_stats()
